#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
#include "MQTTClient.h"
#include <MQTTClientPersistence.h>
#include <time.h>
#include "ADXL345.h"
#include <unistd.h>
#include <pthread.h>


using namespace std;
using namespace exploringRPi;


#define ADDRESS    "tcp://192.168.3.62:1883"  // server ip
#define CLIENTID   "rpi1"
#define AUTHMETHOD "debuser"
#define AUTHTOKEN  "debuser"
#define QOS        0
#define TIMEOUT    10000L


float getCPUTemperature() {        
   int cpuTemp;                    
   fstream fs;                     
   fs.open("/sys/class/thermal/thermal_zone0/temp", fstream::in);
   fs >> cpuTemp;
   fs.close();
   return (((float)cpuTemp)/1000);
}
   

int main(int argc, char* argv[]) {
   
  time_t rawtime;
  struct tm * timeinfo;
  time (&rawtime);
  timeinfo = localtime (&rawtime);

   ADXL345 sensor(1,0x53);
   sensor.setResolution(ADXL345::NORMAL);
   sensor.setRange(ADXL345::PLUSMINUS_4_G);

   char str_payload[100]; 

   MQTTClient client;
   MQTTClient_connectOptions opts = MQTTClient_connectOptions_initializer;
   MQTTClient_message pubmsg = MQTTClient_message_initializer;
   MQTTClient_deliveryToken token;
   
   MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_DEFAULT, NULL);  
   
   MQTTClient_willOptions willmsg    =MQTTClient_willOptions_initializer;  

   opts.keepAliveInterval = 20;
   opts.cleansession = false;
   opts.username = AUTHMETHOD;
   opts.password = AUTHTOKEN;
   
   opts.will=&willmsg;
   
   willmsg.topicName="ee513/test"; 
   willmsg.message="No data from sensor";
   willmsg.qos=QOS;
   willmsg.retained=0;
   
   
   int rc;
   if ((rc = MQTTClient_connect(client, &opts)) != MQTTCLIENT_SUCCESS) {
      cout << "Failed to connect, return code " << rc << endl;
      return -1;
   }

   sprintf(str_payload, "{\"ee513\":{\"Temperature\": %f, \"Time\": %s, \"Pitch\": %f, \"Roll\": %f  }}", 
   getCPUTemperature(),asctime(timeinfo),sensor.sensor_adxl('P'),sensor.sensor_adxl('R'));
   
   pubmsg.payload = str_payload;
   pubmsg.payloadlen = strlen(str_payload);
   pubmsg.qos = QOS;
   pubmsg.retained = 0;
   MQTTClient_publishMessage(client,"ee513/test", &pubmsg, &token);
   cout << "Waiting for up to " << (int)(TIMEOUT/1000) <<
        " seconds for publication of " << str_payload <<
        " \non topic " << "ee513/test"<< " for ClientID: " << CLIENTID << endl;

          
   rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
   cout << "Message with token " << (int)token << " delivered." << endl;
   MQTTClient_disconnect(client, 10000);
   MQTTClient_destroy(&client);
   return rc;

}
